/**********************************************
* File: passByRef.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file includes the first example for the student
* of passing values by reference to a function 
**********************************************/

#include <stdio.h>
#include <stdlib.h> // Included for malloc

/********************************************
* Function Name  : getLetter
* Pre-conditions : char* word, long unsigned int wordSize, char findChar, int* currLetter
* Post-conditions: none
* 
* This file takes in a char array of size wordSize, and determines if the 
* character findChar is in the array. If found, the value of int currLetter will
* be the character's first location in the array 
********************************************/
void getLetter(char* word, long unsigned int wordSize, char findChar, int* currLetter){
	
	for(long unsigned int i = 0; i < wordSize; i++){
		
		// Passing findChar by value. No need to de-reference 
		if(word[i] == findChar){
			
			// De-reference currLetter and update the value 
			// Cast from long unsigned int to currLetter since currLetter may 
			// contain a negative value 
			*currLetter = (int)i;
			
			// Move the function off the stack and return
			return;
		}
	}
	
}


/********************************************
* Function Name  : printResult
* Pre-conditions : char* word, char findChar, int currLetter
* Post-conditions: none
* 
* This function prints the location of the character findChar 
* in the char array word. Must call getLetter previously
* to get a valid result.  
********************************************/
void printResult(char* word, char findChar, int currLetter){
	
	
	if(currLetter == -1){
		fprintf(stdout, "The word %s does not contain the letter %c\n", word, findChar);
	}
	else{
		fprintf(stdout, "The first instance of %c in %s is found at %d\n", findChar, word, currLetter);
	}
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* malloc requires a long unsigned int as input */
	long unsigned int wordSize = 5;

	char* Notre = malloc(wordSize * sizeof(char));
	
	/* Assign each character individually in order to preserve the char* */
	Notre[0] = 'N'; Notre[1] = 'o'; Notre[2] = 't'; Notre[3] = 'r'; Notre[4] = 'e'; 
	
	// Set currLetter to -1, so that if the letter is not found, -1 can be used as false
	int currLetter = -1;	
	
	// We are trying to find the character t
	char findChar = 't';
	
	// Get the location of the letter 
	getLetter(Notre, wordSize, findChar, &currLetter);
	
	// Print the result to the user 
	printResult(Notre, findChar, currLetter);
	
	// Free the char* pointer
	free(Notre);

	return 0;
}

