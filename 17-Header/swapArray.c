/**********************************************
* File: passByRef.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file includes the first example for the student
* of passing values by reference to a function 
**********************************************/

#include <stdio.h>
#include <stdlib.h> // Included for malloc

/********************************************
* Function Name  : swapInts
* Pre-conditions : int* word, long unsigned int numInts, int a, int b
* Post-conditions: none
* 
* Swaps the values of a and b in the array 
********************************************/
void swapInts(int* word, long unsigned int numInts, int a, int b){

	/* Error Checking */
	if(a < 0 || b < 0){
		fprintf(stderr, "Invalid input to swapInts. Less than 0.\n");
		return;
	}
	
	if(a >= (int)numInts || b >= (int)numInts){
		fprintf(stderr, "Invalid input to swapInts. Exceeds array length.\n");
		return;	
	}
	
	/* Swap valid array locations */
	fprintf(stdout, "Before: word[%d] = %d and word[%d] = %d\n", a, word[a], b, word[b]);
	
	int temp = word[a];
	word[a] = word[b];
	word[b] = temp;
	
	fprintf(stdout, "After: word[%d] = %d and word[%d] = %d\n", a, word[a], b, word[b]);
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	long unsigned int numInts = 5;
	
	int* SwapInt = malloc(numInts * sizeof(int));
	
	int i;
	for(i = 0; i < (int)numInts; i++){
		SwapInt[i] = i*2;
	}
	
	swapInts(SwapInt, numInts, 0, i-1);
	
	swapInts(SwapInt, numInts, -1, i-1);
	
	swapInts(SwapInt, numInts, 0, i);
	
	swapInts(SwapInt, numInts, 1, i-2);
	
	free(SwapInt);

	return 0;
}

