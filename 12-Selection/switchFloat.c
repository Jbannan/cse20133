/**********************************************
* File: switchFloat.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of a switch statement
* using a 32-bit signed integer 
**********************************************/

#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable Declaration */
	float NDClass = 1;
	
	switch(NDClass){
		
		case 0:
			fprintf(stdout, "Student is a Freshman!\n");
			break;
			
		case 1:
			fprintf(stdout, "Student is a Sophomore!\n");
			break;
			
		case 2:
			fprintf(stdout, "Student is a Junior!\n");
			break;
			
		case 3:
			fprintf(stdout, "Student is a Senior!\n");
			break;
			
		default:
			fprintf(stdout, "Student is a Super-Senior!\n");
			break;
		
	}

	return 0;
}
