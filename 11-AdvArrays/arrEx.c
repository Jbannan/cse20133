/**********************************************
* File: arrEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Examples of accessing arrays and array arithmetic
**********************************************/

#include <stdio.h>
#include <stdlib.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	int First[] = {78, 111, 116, 114, 101};
	int Second[] = {'D', 'a', 'm', 'e'};
	
	int sum = First[0] + *(Second + 3);
	
	fprintf(stdout, "The sum of %d and %d is %d\n", *(First + 0), Second[3], sum);
	
	*(Second + 1) = sum; 
	
	fprintf(stdout, "New value is %d\n", Second[1]);

	return 0;
}
