/**********************************************
* File: double.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of doubles  
**********************************************/

#include "stdio.h"
#include "math.h"	/* Used for pow function */

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable declarations */
	double smalldouble = -1.0125 * pow(2, -205);
	double slidedouble = 0.15625;
	double largedouble = 1.8725 * pow(2, 205);
	
	fprintf(stdout, "Decimal smalldouble: %.67lf\n", smalldouble);
	fprintf(stdout, "Scientific smalldouble: %e\n", smalldouble);
	fprintf(stdout, "Hexadecimal smalldouble: %a\n\n", smalldouble);
	
	fprintf(stdout, "Decimal slidedouble: %lf\n", slidedouble);
	fprintf(stdout, "Scientific slidedouble: %e\n", slidedouble);
	fprintf(stdout, "Hexadecimal slidedouble: %a\n\n", slidedouble);
	
	fprintf(stdout, "Decimal largedouble: %lf\n", largedouble);
	fprintf(stdout, "Scientific largedouble: %e\n", largedouble);
	fprintf(stdout, "Hexadecimal largedouble: %a\n\n", largedouble);

	return 0;
}

