/**********************************************
* File: sprite.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a simple example of bit masking
* to show how values are replaced in a video game 
**********************************************/

#include <stdio.h>

int main(void){

	/* Five random Sprites */
	unsigned int sprite0 = 0x00D35AB9;	/* https://www.colorhexa.com/d35ab9 */
	unsigned int sprite1 = 0x00DA9127;	/* https://www.colorhexa.com/da9127 */
	unsigned int sprite2 = 0x00A71392;	/* https://www.colorhexa.com/a71392 */
	unsigned int sprite3 = 0x000001B3;	/* https://www.colorhexa.com/0001b3 */
	unsigned int sprite4 = 0x000B791A;	/* https://www.colorhexa.com/0B791A */
	
	/* RGB for Black Color */
	unsigned int blackOut = 0x00000000;
	unsigned int whiteOut = 0xFFFFFFFF;
	
	/* Official colors from ND On Message - https://onmessage.nd.edu/athletics-branding/colors/ */
	unsigned int NDBlue = 0x000C2340;
	unsigned int NDGold = 0x00C99700;
	unsigned int NDGreen = 0x0000843D;
	
	/* Step 1 Transparent Raster. Black where we want to replace. White where it stays the same */
	unsigned int rasterTransp0 = blackOut;
	unsigned int rasterTransp1 = whiteOut;
	unsigned int rasterTransp2 = blackOut;
	unsigned int rasterTransp3 = whiteOut;
	unsigned int rasterTransp4 = blackOut;
	
	/* Step 2 Overlay Raster. Colors where we want to replace. Black where it stays the same */
	unsigned int rasterOver0 = NDBlue;
	unsigned int rasterOver1 = blackOut;
	unsigned int rasterOver2 = NDGold;
	unsigned int rasterOver3 = blackOut;
	unsigned int rasterOver4 = NDGreen;
	
	fprintf(stdout, "Pre-Black out: %x/%x/%x/%x/%x\n\n", sprite0, sprite1, sprite2, sprite3, sprite4);

	/* Part 1 - Black out sprites 0, 2 and 4 */
	sprite0 = sprite0 & rasterTransp0;
	sprite1 = sprite1 & rasterTransp1;
	sprite2 = sprite2 & rasterTransp2;
	sprite3 = sprite3 & rasterTransp3;
	sprite4 = sprite4 & rasterTransp4;
	
	fprintf(stdout, "Black out    : %x/%x/%x/%x/%x\n\n", sprite0, sprite1, sprite2, sprite3, sprite4);
	
	/* Part 2 - Replace colors */
	sprite0 = sprite0 | rasterOver0;
	sprite1 = sprite1 | rasterOver1;
	sprite2 = sprite2 | rasterOver2;
	sprite3 = sprite3 | rasterOver3;
	sprite4 = sprite4 | rasterOver4;
	
	fprintf(stdout, "With ND      : %x/%x/%x/%x/%x\n\n", sprite0, sprite1, sprite2, sprite3, sprite4);

	return 0;
}

