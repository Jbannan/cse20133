/**********************************************
* File: longIntMax.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* This file shows an example of when not using strict 
* compiling flags and allowing warnings can lead to issues
* This file is complementary to longIntMaxBad.c. In this file, we use the correct 
* specifiers for long ints %ld and %lx instead of %d and %x
* The makefile also compiled using Production Quality compiler flags 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void){
	
	/* Variable declaration */
	long int biggest32 = 2147483647;

	/* Teaching students about initializing long int */
	fprintf(stdout, "Calculating integer sum...\n");

	fprintf(stdout, "The decimal value of 2^32 - 1 signed integer sum is: %ld\n", biggest32);
	fprintf(stdout, "The hex value of 2^32 - 1 signed integer sum is:     %lx\n\n", biggest32);
	
	fprintf(stdout, "The decimal value of (2^32 - 1)*4 signed integer sum is: %ld\n", biggest32 * 4);
	fprintf(stdout, "The hex value of (2^32 - 1)*4 signed integer sum is:     %lx\n", biggest32 * 4);
	
	fprintf(stdout, "\nCalculating adding 1...\n");

	biggest32++;
	
	fprintf(stdout, "The decimal value of (2^32 - 1) + 1 signed integer sum is: %ld\n", biggest32);
	fprintf(stdout, "The hex value of (2^32 - 1) + 1 signed integer sum is:     %lx\n", biggest32);

	return 0;
}
