/**********************************************
* File: longIntMaxBad.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* This file shows an example of when not using strict 
* compiling flags and allowing warnings can lead to issues
* This file is complementary to longIntBad.c. In this file, 
* we use the incorrect specifiers %d and %x
*
* When running longIntMaxBad from the Makefile, this code will not compile
* To run with warnings and getting bad results, run the following
* gcc -Wall -ansi longIntMaxBad.c -o longIntMaxBad
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void){

	/* Variable declaration */
	long int biggest32 = 2147483647;

	/* Teaching students about initializing long int */
	fprintf(stdout, "Calculating integer sum...\n");

	fprintf(stdout, "The decimal value of 2^32 - 1 signed integer sum is: %d\n", biggest32);
	fprintf(stdout, "The hex value of 2^32 - 1 signed integer sum is:     %x\n\n", biggest32);
	
	fprintf(stdout, "The decimal value of (2^32 - 1)*4 signed integer sum is: %d\n", biggest32 * 4);
	fprintf(stdout, "The hex value of (2^32 - 1)*4 signed integer sum is:     %x\n", biggest32 * 4);
	
	fprintf(stdout, "\nCalculating adding 1...\n");

	biggest32++;
	
	fprintf(stdout, "The decimal value of (2^32 - 1) + 1 signed integer sum is: %d\n", biggest32);
	fprintf(stdout, "The hex value of (2^32 - 1) + 1 signed integer sum is:     %x\n", biggest32);

	return 0;
}
