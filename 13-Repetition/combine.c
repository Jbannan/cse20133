/**********************************************
* File: combine.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of arrays, pointer
* arithmetic, if/else statements, and switch statements 
**********************************************/

#include <stdlib.h>
#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Declare variables */
	unsigned long int arrayLen = 8;
	int smaller[] = {1, 2, 4, 7, 1842, 2, 10, 19};
	int *larger = malloc(arrayLen * sizeof(int));
	
	/* Insert values into larger individually */
	larger[0] = 1000;
	*(larger + 1) = 57702;
	larger[2] = 47577;
	larger[3] = 1000;
	*(larger + 4) = 0;
	larger[5] = 47577;
	larger[6] = 1000;
	*(larger + 7) = 57702;

	int location = -1;
	int arrayIndex;
	
	/* For loop to calculate result of smaller vs larger */
	for(arrayIndex = 0; arrayIndex < (int)arrayLen; arrayIndex++){
		
		if(smaller[arrayIndex] > *(larger + arrayIndex)){
			location = arrayIndex;
			break; 
		}
		
	}
	
	/* print result to user */
	if(location != -1){
		fprintf(stdout, "The location is %d, and smaller[%d] = %d\n", 
			location, location, *(smaller + location));
	}
	else{
		fprintf(stdout, "Location not found\n");
	}
	
	/* Free allocated memory */
	free(larger);

	return 0;
}