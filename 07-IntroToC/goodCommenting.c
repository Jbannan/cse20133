/********************************************
* Function Name  : sort
* Pre-conditions : int inArray[], int inSize
* Post-conditions: none
* 
* Implements insertion sort on an array of 
* integers of length inSize
********************************************/
void sort(int inArray[], int inSize)
{
	/* Start at the position and examine each element */
	for (int i = 1; i < inSize; i++) {
		int element = inArray[i];

		/* j marks the position of the sorted part */
		int j = i – 1;

		/* While the current slot in the sorted part
		 * is higher than the element, shift over
		 * and move backwards
		 */
		while (j >= 0 && inArray[j] > element) {
			inArray[j+1] = inArray[j];
			j--;
		}

		/* The position of element is found when the while 
		 * condition is broken */
		inArray[j+1] = element;
	}
}


