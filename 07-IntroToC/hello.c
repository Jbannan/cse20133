/**********************************************
* File: hello.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the classic Hello, World program 
* This edit for the PuTTy extra lecture
* This edit is on the local SSH client
* This edit is on the Desktop
**********************************************/

#include <stdio.h> /* fprintf */

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	fprintf(stdout, "Hello, World\n");

	return 0;
}
