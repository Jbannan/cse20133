/**********************************************
* File: charEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains fundamental use of char and its relationship
* to its ASCII value 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void){
	
	char charExample = 'n';
	unsigned char unsCharExample = 0x6f;
	unsigned char largeChar = 255;
	
	fprintf(stdout, "charExample char is  %c\n", charExample);
	fprintf(stdout, "charExample ASCII is %d\n", charExample);
	fprintf(stdout, "charExample hex is   %x\n\n", charExample);
	
	fprintf(stdout, "unsCharExample char is  %c\n", unsCharExample);
	fprintf(stdout, "unsCharExample ASCII is %d\n", unsCharExample);
	fprintf(stdout, "unsCharExample hex is   %x\n\n", unsCharExample);
	
	fprintf(stdout, "largeChar char is  %c\n", largeChar);
	fprintf(stdout, "largeChar ASCII is %d\n", largeChar);
	fprintf(stdout, "largeChar hex is   %x\n\n", largeChar);
	
	int addChar = unsCharExample + largeChar;
	
	fprintf(stdout, "addChar char is  %c\n", addChar);
	fprintf(stdout, "addChar ASCII is %d\n", addChar);
	fprintf(stdout, "addChar hex is   %x\n\n", addChar);
	
	return 0;
}
